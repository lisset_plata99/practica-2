class Arbol {
    constructor() {
        this.nodoPadre = ""; //para agregar un nodo padre
        this.nodos = [];
        //this.nivel = 5; //se le pone el maximo nivel que tendra el arbol 
        //this.busquedaElemento;//aqui se va a guardar el nodo donde se tiene el valor que se esta buscando
        //this.buscarNodos = [];//aqui se van guardando los nombres de los nodos que coincidan con el nivel 5
    }

    agregarNodoPadre(padre, posicion,nombre,valor, nivel){
        let nodo = new Nodo(padre, posicion,nombre,valor, nivel);
         return nodo;
        }
    

    agregarNodo(nodoPadre,posicion,nombre,valor,nivel){
        var nodo = new Nodo(nodoPadre,posicion,nombre,valor,nivel);
        this.nodos.push(nodo);
       return nodo;
    }


    agregarNuevoNodo(valor,nombre){
        let nodo = new Nodo(null,null,nombre,valor,null);
        if(this.nodos.length >= 0){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if(parseInt(nodo.valor) < parseInt(this.nodoPadre.valor)){

                nodo.posicion = "hIzq";
            }
            else{
                nodo.posicion = "hDer";
            }
            this.nodos.push(nodo);
        }
    }

    buscarNodosPadre(padre){
        let nodoEncontrados = [];
        for(let x = 0; x < this.nodos; x++){
            if(this.nodos[x].padre == padre){
                nodoEncontrados.push(this.nodos[x]);

            }
        }
        return nodoEncontrados;
    }

    buscarValor(nombreElemento,valorElemento,nodo){
        camino = camino+nodo.valor;
        if(nodo.valor == valorElemento && nodo.nombre == nombreElemento)
        {
            this.busquedaElemento = nodo;
            totalCamino = camino;
            return this.busquedaElemento;
        }

        if(nodo.hI != undefined)
            this.buscarValor(nombreElemento,valorElemento,nodo.hI);// (es como  un tipo foreach)

        if(nodo.hD != undefined)
            this.buscarValor(nombreElemento,valorElemento,nodo.hD);

        return this.busquedaElemento;//retorna el nodo que tenga el valor buscado
    }










}